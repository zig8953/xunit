using System;
using Xunit;
using Client.Service;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Moq;
using Models;
using System.Collections.Generic;
using System.Linq;
using Task17RESTApi.Data;
using Task17RESTApi.Controllers;

namespace XUnitTest
{
    public class UnitTest
    {

       [Fact]
        public void loadConfig()
        {
           // IConfiguration Configuration = new ConfigurationBuilder()
           //.AddJsonFile("settings.json", optional: true, reloadOnChange: true)
           //.AddEnvironmentVariables()
           //.Build();

           // Assert.Equal("https://localhost:44389/", Configuration.GetSection("URL").Value);
        }


        [Fact]
        public void createUserTest()
        {
            //IConfiguration Configuration = new ConfigurationBuilder()
            //.AddJsonFile("settings.json", optional: true, reloadOnChange: true)
            //.AddEnvironmentVariables()
            //.Build();

            HttpClient httpClient = new HttpClient();
            UserService userService = new UserService(httpClient);

            httpClient.BaseAddress = new Uri("https://localhost:44389/");
            httpClient.DefaultRequestHeaders.Add("User-Agent", "ClientServer");

            User user = new User() { FullName = "Test 1", Id = 1 };

            Assert.NotNull(userService.Create(user));
        }

        [Fact]
        public void getUserTest()
        {
            //IConfiguration Configuration = new ConfigurationBuilder()
            //.AddJsonFile("settings.json", optional: true, reloadOnChange: true)
            //.AddEnvironmentVariables()
            //.Build();

            HttpClient httpClient = new HttpClient();
            UserService userService = new UserService(httpClient);

            httpClient.BaseAddress = new Uri("https://localhost:44389/");
            httpClient.DefaultRequestHeaders.Add("User-Agent", "ClientServer");

            Assert.NotNull(userService.Get(1));
        }
    }
}
