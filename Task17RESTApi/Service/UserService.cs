﻿using Models;
using System.Linq;
using Task17RESTApi.Data;

namespace Task17RESTApi.Service
{
    public class UserService : IUserService
    {
        private readonly SQLLiteContext _context;
        public UserService(SQLLiteContext context)
        {
            _context = context;
        }
        public bool Create(User user)
        {
            if (_context.users.Where(u => u.Id == user.Id).Any())
            {
                return false;   
            }
            _context.users.Add(user);
            if (_context.SaveChanges() > 0)
                return true;
            else
                return false; 
        }

        public User Get(int id)
        {
            return _context.users.Where( qq => qq.Id == id).FirstOrDefault();    
        }
    }
}
