﻿using Models;
using System;
using System.Net.Http;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
namespace Client
{
    class Program
    {
       
        static  void Main(string[] args)
        {
            IConfiguration Configuration = new ConfigurationBuilder()
            .AddJsonFile("settings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .AddCommandLine(args)
            .Build();

            User user;
            HttpClient httpClient = new HttpClient();
            Service.UserService userService = new Service.UserService(httpClient);
            httpClient.BaseAddress = new Uri(Configuration.GetSection("URL").Value);
            httpClient.DefaultRequestHeaders.Add("User-Agent", "ClientServer");

            for (int i = 1; i < 10; i++)
            {
                user = new User() {Id = i, FullName = $"Test{i}"};
                Console.WriteLine(userService.Create(user).Result);
            }

            for (int i = 1; i < 10; i++)
            {
                user = userService.Get(i).Result;
                Console.WriteLine($"Id: {user.Id} FullName: {user.FullName}");
            }
        }
    }
}
